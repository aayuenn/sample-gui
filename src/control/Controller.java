package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.BankAccount;
import view.InvestmentFrame;

public class Controller {
	private BankAccount ba;
	private InvestmentFrame frame;
	private double DEFAULT_RATE = 5;
	
	
	public Controller(BankAccount ba, InvestmentFrame frame) {
		super();
		this.ba = ba;
		this.frame = frame;
		this.frame.setButtonListener(new AddInterestListener());
		this.frame.setBalance(ba.getBalance());
		this.frame.setRate(DEFAULT_RATE);
	}



	public Controller() {
		super();
	}



	class AddInterestListener implements ActionListener
    {
       public void actionPerformed(ActionEvent event)
       {
          double rate = Double.parseDouble(frame.getRate());
          double interest = ba.getBalance() * rate / 100;
          ba.deposit(interest);
          frame.setBalance(ba.getBalance());
       }            
    }
}
